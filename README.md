# Teamprojekt

Dieses Programm bietet die Möglichkeit einen [Fernsehlift](https://www.reichelt.de/motorisierer-einbaulift-37-65-94-165-cm--myw-hp33-2l-p271300.html?PROVID=2788) alternativ mittels MQTT, einem ESP32 und einem NRF24 zu steuern.

## Konfiguration

In der Datei [mqtthandler.h](./include/mqttHandler.h) müssen die folgenden Werte entsprechend der Anwendung definiert werden:

|Variable       |Beschreinung                                            |
|---------------|--------------------------------------------------------|
|ssid           |SSID des Netwerkes, in dem sich der MQTT-Broker befindet|
|password       |Password für das Netzwerk                               |
|mqtt_server    |IP-Adresse des MQTT-Brokers                             |
|mqttUser       |Benutzername des MQTT-Brokers                           |
|mqttPassword   |Password des MQTT-Brokers                               |

## Anschluss NRF24
Der NRF24 muss entsprechend der folgenden Tabelle an den ESP-32 angeschlossen werden.


|NRF24 (Pin Nummer - Pin Name)|Beschreibung |ESP32 (Pin Nummer - Pin Name)|
|----------|---------------------|----------|
|1- Ground |Ground               | 2 - GND  |
|2 - VCC   | Versorgungsspannung | 1-3V3    |
|3 - CE    | Chip Enable         | 5 - IO4  |
|4 - CSN   | Ship Select Not     | 8 - O5   |
|5 - SCK   | Serial Clock        | 9 - IO18 |
|6 - MOSI  | Master Out Slave In | 15 - IO23| 
|7 - MISO  | Master In Slave Out |10 - IO19 |
|8 - IRQ   | Interrupt          |    -      |

## MQTT-Topics

Die folgende Tabelle enthält die Topics, mit welchen der Lift gesteuert werden kann.

|Beschreibung               | Topic                 | Befehle|
|---------------------------|-----------------------|--------|
|Manuelles Hochfahren       | lift/command/up       | On/Off |
|Manuelles Herunterfahren   | lift/command/down     | On/Off |
|Position 1 anfahren        | lift/command/button1  | On/Off |
|Position 2 anfahren        | lift/command/button2  | On/Off |
|Speichern als Position 1   | lift/command/mem1     | On/Off |
|Speichern als Position 2   | lift/command/mem2     | On/Off |
|Verbidungsaufbau           | lift/command/set      | On/Off |
|Unterste Position anfahren | lift/command/ok       | On/Off |
|Nicht belegt               | lift/command/left     | On/Off |
|Nicht belegt               | lift/command/right    | On/Off |