#include "mqttHandler.h"

int mqttHandler::topicStateButton1 = 0;
int mqttHandler::topicStateButton2 = 0;
int mqttHandler::topicStateUp = 0;
int mqttHandler::topicStateDown = 0;
int mqttHandler::topicStateLeft = 0;
int mqttHandler::topicStateRight = 0;
int mqttHandler::topicStateMem1 = 0;
int mqttHandler::topicStateMem2 = 0;
int mqttHandler::topicStateSet = 0;
int mqttHandler::topicStateOk = 0;

mqttHandler::mqttHandler()
{
  
}

void mqttHandler::setup() 
{
  delay(10);

  connectToWifi();

  client.setServer(mqttServer.c_str(), 1883);
  client.setCallback(callback);
  Serial.println("");
}

void mqttHandler::callback(char* topic, uint8_t* message, unsigned int length) 
{
  Serial.print("Message arrived: ");
  Serial.print(topic);
  Serial.print("| Message: ");
  String messageTemp;

  for (int i = 0; i < length; i++) 
  {
    messageTemp += (char)message[i];
  }

  Serial.println(messageTemp);

  if (String(topic) == TOPIC_NAME_BUTTON1) 
  {
    if (messageTemp == "ON") 
    {
      topicStateButton1 = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateButton1 = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_BUTTON2) 
  {
    if (messageTemp == "ON") 
    {
      topicStateButton2 = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateButton2 = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_UP) 
  {
    if (messageTemp == "ON") 
    {
      topicStateUp = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateUp = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_DOWN) 
  {
    if (messageTemp == "ON") 
    {
      topicStateDown = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateDown = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_LEFT) 
  {
    if (messageTemp == "ON") 
    {
      topicStateLeft = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateLeft = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_RIGHT) 
  {
    if (messageTemp == "ON") 
    {
      topicStateRight = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateRight = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_MEM1) 
  {
    if (messageTemp == "ON")
    {
      topicStateMem1 = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateMem1 = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_MEM2) 
  {
    if (messageTemp == "ON") 
    {
      topicStateMem2 = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateMem2 = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_OK) 
  {
    if (messageTemp == "ON") 
    {
      topicStateOk = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateOk = 0;
    }
  }
  else if (String(topic) == TOPIC_NAME_SET) 
  {
    if (messageTemp == "ON")
    {
      topicStateSet = 1;
    } 
    else if (messageTemp == "OFF") 
    {
      topicStateSet = 0;
    }
  }
}

boolean mqttHandler::mqttConnectionStatus() 
{
  return client.connected();
}

void mqttHandler::loopMqtt()
{
  client.loop();
}

void mqttHandler::connectMqttBroker() 
{
  bool connectionSuccess = false;

  if(mqttUser.isEmpty() && mqttPassword.isEmpty())
  {
    connectionSuccess = client.connect(mqttID.c_str(), NULL, NULL);
  }
  else
  {
    connectionSuccess = client.connect(mqttID.c_str(), mqttUser.c_str(), mqttPassword.c_str());
  }

  if (connectionSuccess) 
  {
    client.subscribe(TOPIC_NAME_BUTTON1);
    client.subscribe(TOPIC_NAME_BUTTON2);
    client.subscribe(TOPIC_NAME_UP);
    client.subscribe(TOPIC_NAME_DOWN);
    client.subscribe(TOPIC_NAME_LEFT);
    client.subscribe(TOPIC_NAME_RIGHT);
    client.subscribe(TOPIC_NAME_MEM1);
    client.subscribe(TOPIC_NAME_MEM2);
    client.subscribe(TOPIC_NAME_SET);
    client.subscribe(TOPIC_NAME_OK);
    Serial.println("");
  } 
}

void mqttHandler::connectToWifi()
{
  if(WiFi.begin(ssid, password) == WL_CONNECTED)
  {

  }
}

bool mqttHandler::wifiConnectionStatus()
{
  return WiFi.status() == WL_CONNECTED;
}

void mqttHandler::printDebugInformationWIFI()
{
  Serial.println("---------------------------------");
  Serial.println("Printing Debug Information WIFI:");
  Serial.println("");
  Serial.print("SSID: ");
  Serial.println(ssid);
  Serial.print("Password: ");
  Serial.println(password);
  Serial.println("");
  Serial.print("Connection Status: ");
  
  switch(WiFi.status())
  {
    case 255:
      Serial.println("WL_NO_SHIELD");
      break;
    case 0:
      Serial.println("WL_IDLE_STATUS");
      break;
    case 1:
      Serial.println("WL_NO_SSID_AVAIL ");
      break;
    case 2:
      Serial.println("WL_SCAN_COMPLETED");
      break;
    case 3:
      Serial.println("WL_CONNECTED");
      Serial.print("Local-IP: ");
      Serial.println(WiFi.localIP());
      break;
    case 4:
      Serial.println("WL_CONNECT_FAILED");
      break;
    case 5:
      Serial.println("WL_CONNECTION_LOST");
      break;
    case 6:
      Serial.println("WL_DISCONNECTED");
      break;
    default:
      break;
  }

  Serial.println("");
  Serial.println("---------------------------------");
}

void mqttHandler::printDebugInformationMQTT()
{
  Serial.println("---------------------------------");
  Serial.println("Printing Debug Information MQTT:");
  
  Serial.println("");
  Serial.print("Server: ");
  Serial.println(mqttServer);
  Serial.print("User: ");
  Serial.println(mqttUser);
  Serial.print("Password: ");
  Serial.println(mqttPassword);
  Serial.println("");

  Serial.print("Connection Status: ");

  switch(client.state())
  {
    case -4:
      Serial.println("MQTT_CONNECTION_TIMEOUT");
      break;
    case -3:
      Serial.println("MQTT_CONNECTION_LOST");
      break;
    case -2:
      Serial.println("MQTT_CONNECT_FAILED ");
      break;
    case -1:
      Serial.println("MQTT_DISCONNECTED");
      break;
    case 0:
      Serial.println("MQTT_CONNECTED");
      break;
    case 1:
      Serial.println("MQTT_CONNECT_BAD_PROTOCOL");
      break;
    case 2:
      Serial.println("MQTT_CONNECT_BAD_CLIENT_ID");
      break;
    case 3:
      Serial.println("MQTT_CONNECT_UNAVAILABLE");
      break;
    case 4:
      Serial.println("MQTT_CONNECT_BAD_CREDENTIALS");
      break;
    case 5:
      Serial.println("MQTT_CONNECT_UNAUTHORIZED");
      break;
    default:
      break;
  }

  Serial.println("");
  Serial.println("---------------------------------");
}