#include "serialHandler.h"

serialHandler::serialHandler()
{

}

void serialHandler::setup()
{
    Serial.begin(BAUD_RATE);
    delay(200);

    Serial.println("Teamprojekt Ostfalia Wolfenbüttel");
    Serial.println("---------------------------------");

    configuration.begin("credentials", false);

    loadMemoryToValues();

    configuration.end();

    showcaseLift.setupNRF24();

    mqttBroker.setup();

    mqttBroker.connectMqttBroker();

    Serial.println("Finished Setup entering standard operation mode");
    Serial.println("Press P to enter programming mode");

    enterStandardMode();
}

void serialHandler::loadMemoryToValues()
{
    mqttBroker.ssid = configuration.getString("ssid");
    mqttBroker.password = configuration.getString("password");
    mqttBroker.mqttServer = configuration.getString("mqttServer");
    mqttBroker.mqttUser = configuration.getString("mqttUser");
    mqttBroker.mqttPassword = configuration.getString("mqttPassword");

    delay(1000);
}

void serialHandler::writeStringBuffer(String address)
{
    configuration.begin("credentials", false);

    configuration.putString(address.c_str(), stringBuffer);

    loadMemoryToValues();

    mqttBroker.connectToWifi();

    Serial.println("Finished Writing new Value to Memory");

    Serial.println("---------------------------------");

    configuration.end();
}

void serialHandler::enterProgrammingMode()
{
    Serial.println("---------------------------------");
    Serial.println("");
    Serial.println("Entered Programming Mode");
    Serial.println("Press 1 to set a SSID for the WIFI");
    Serial.println("Press 2 to set a Password for the WIFI");
    Serial.println("Press 3 to set a IP-Address for the MQTT-Broker");
    Serial.println("Press 4 to set a Username for the MQTT-Broker");
    Serial.println("Press 5 to set a Password for the MQTT-Broker");
    Serial.println("Press 6 to display debug information about the WIFI");
    Serial.println("Press 7 to display debug information about the MQTT");
    Serial.println("Press 8 to display debug information about the NRF24");
    Serial.println("Press q to quit programming mode");
    Serial.println("");
    Serial.println("---------------------------------");

    while(1)
    {
        if(Serial.available())
        {
            switch (Serial.read())
            {
            case '1':
                enterValue("ssid");
                return;
                break;
            case '2':
                enterValue("password");
                return;
                break;
            case '3':
                enterValue("mqttServer");
                return;
                break;
            case '4':
                enterValue("mqttUser");
                return;
                break;
            case '5':
                enterValue("mqttPassword");
                return;
                break;
            case '6':
                mqttBroker.printDebugInformationWIFI();
                return;
                break;
            case '7':
                mqttBroker.printDebugInformationMQTT();
                return;
                break;
            case '8':
                showcaseLift.printDebugInformation();
                return;
                break;
            case 'q':
                return;
                break;
            default:
                break;
            }
        }
    }
}

void serialHandler::enterStandardMode()
{
    Serial.println("Entered mode for standard execution");

    while(1)
    {
        if(Serial.available())
        {
            if(Serial.read() == 'p')
            {
                enterProgrammingMode();
            }
        }

        if (mqttBroker.mqttConnectionStatus())
        {
            int singleButtonPressed = mqttHandler::topicStateButton1 + mqttHandler::topicStateButton2 
                                            + mqttHandler::topicStateUp + mqttHandler::topicStateDown 
                                            + mqttHandler::topicStateLeft + mqttHandler::topicStateRight 
                                            + mqttHandler::topicStateMem1 + mqttHandler::topicStateMem2 
                                            + mqttHandler::topicStateSet + mqttHandler::topicStateOk;

            
            if (singleButtonPressed == 1)
            {
                if (mqttHandler::topicStateButton1)
                {
                    showcaseLift.controlButton1();
                }

                if (mqttHandler::topicStateButton2)
                {
                    showcaseLift.controlButton2();
                }

                if (mqttHandler::topicStateUp)
                {
                    showcaseLift.controlUp();
                }

                if (mqttHandler::topicStateDown)
                {
                    showcaseLift.controlDown();
                }

                if (mqttHandler::topicStateLeft)
                {
                    showcaseLift.controlLeft();
                }

                if (mqttHandler::topicStateRight)
                {
                    showcaseLift.controlRight();
                }

                if (mqttHandler::topicStateMem1)
                {
                    showcaseLift.controlMem1();
                }

                if (mqttHandler::topicStateMem2)
                {
                    showcaseLift.controlMem2();
                }

                if (mqttHandler::topicStateSet)
                {
                    showcaseLift.controlSet();
                }

                if (mqttHandler::topicStateOk)
                {
                    showcaseLift.controlButtonOk();
                }
            }
        }
        else
        {   
            mqttBroker.connectMqttBroker();
            delay(500);
        }

        if (!mqttBroker.wifiConnectionStatus())
        {
            mqttBroker.connectToWifi();
            delay(500);
        }

        mqttBroker.loopMqtt();
    }
}

void serialHandler::enterValue(String address)
{
    Serial.println("Enter new value for the selected parameter:");

    stringBuffer = "";

    while(1)
    {
        if(Serial.available())
        {
            char currentChar = Serial.read();

            if(currentChar == 13)
            {
                Serial.println("");
                writeStringBuffer(address);
                return;
            }
            else
            {
                if (currentChar == 127)
                {
                    if(stringBuffer.length() > 0)
                    {
                        Serial.print(currentChar);
                        stringBuffer.remove(stringBuffer.length() - 1);
                    }
                }
                
                if (currentChar > 32 && !(currentChar == 127))
                {
                    Serial.print(currentChar);
                    stringBuffer += currentChar;
                }
            }
        }
    }
}