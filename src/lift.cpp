#include "lift.h"

lift::lift()
{

}

void lift::setupNRF24()
{
    radio.begin();
    radio.setPALevel(RF24_PA_MIN);
    radio.setDataRate(RF24_1MBPS);

    radio.setAutoAck(false);

    radio.setPayloadSize(PKT_SIZE);

    radio.setChannel(CHANNEL);

    radio.stopListening();
    radio.openWritingPipe(ADDRESS);
}

void lift::sendCommand(uint8_t* buf)
{
    for(int i = 0; i < COMMAND_REPEATS; i++)
    {
        radio.write(buf, PKT_SIZE, false);
        delay(40);
    }

    Serial.println("Sending the MQTT-Command to the lift");
}

void lift::controlUp()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 125, 176, 0};    
    sendCommand(command);
}

void lift::controlDown()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 126, 177, 0};    
    sendCommand(command);
}

void lift::controlLeft()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 126, 48, 0};    
    sendCommand(command);
}

void lift::controlRight()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 121, 172, 0};    
    sendCommand(command);
}

void lift::controlSet()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 63, 242, 0};    
    sendCommand(command);
}

void lift::controlMem1()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 119, 170, 0};    
    sendCommand(command);
}

void lift::controlMem2()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 123, 174, 0};    
    sendCommand(command);
}

void lift::controlButton1()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 127, 49, 255};    
    sendCommand(command);
}

void lift::controlButton2()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 115, 166, 0};    
    sendCommand(command);
}

void lift::controlButtonOk()
{
    uint8_t command[PKT_SIZE] = {153, 80, 119, 124, 175, 0};    
    sendCommand(command);
}

void lift::printDebugInformation()
{
    Serial.println("---------------------------------");
    Serial.println("Printing Debug Information NRF24:");
    Serial.print("");
    Serial.print("Connection status: ");

    if(radio.isChipConnected())
    {
        Serial.println("CONNECTED");
    }
    else
    {
        Serial.println("FAILED");
    }

    Serial.println("");

    radio.printPrettyDetails();
    Serial.println("---------------------------------");
}