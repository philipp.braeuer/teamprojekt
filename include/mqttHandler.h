#ifndef MQTT_H
#define MQTT_H
#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include "lift.h"

class mqttHandler
{
private:
    // Verweise zu den Topics des MQTT-Brokers für die Befehle
    static constexpr char const *TOPIC_NAME_BUTTON1 = "lift/command/button1";
    static constexpr char const *TOPIC_NAME_BUTTON2 = "lift/command/button2";
    static constexpr char const *TOPIC_NAME_UP = "lift/command/up";
    static constexpr char const *TOPIC_NAME_DOWN = "lift/command/down";
    static constexpr char const *TOPIC_NAME_LEFT = "lift/command/left";
    static constexpr char const *TOPIC_NAME_RIGHT = "lift/command/right";
    static constexpr char const *TOPIC_NAME_MEM1 = "lift/command/mem2";
    static constexpr char const *TOPIC_NAME_MEM2 = "lift/command/mem1";
    static constexpr char const *TOPIC_NAME_SET = "lift/command/set";
    static constexpr char const *TOPIC_NAME_OK = "lift/command/ok";

    // ID für die Unterescheidung der Ansteuerschaltung im MQTT-Broker
    String mqttID = "Ansteuerschaltung";

    WiFiClient espClient{};
    PubSubClient client{espClient};

    // Callback-Funktion die aufgerufen wird, sobald ein Topic geupdatet wurde
    static void callback(char* topic, uint8_t* message, unsigned int length);
public:
    // Variablen für das lokale zwischenspeichern der Zugangsdaten für den
    // MQTT-Broker und die WIFI-Verbindung
    String ssid = "Teamprojekt";     
    String password = "Teamprojekt"; 

    String mqttServer = "10.3.141.1";
    String mqttUser = "Teamprojekt"; 
    String mqttPassword = "Teamprojekt";   

    // Variablen erkennen zu können, ob ein Topic den Wert verändert hat
    static int topicStateButton1;
    static int topicStateButton2;
    static int topicStateUp;
    static int topicStateDown;
    static int topicStateLeft;
    static int topicStateRight;
    static int topicStateMem1;
    static int topicStateMem2;
    static int topicStateSet;
    static int topicStateOk;

    mqttHandler();

    // Setup-Methode für das aufbauen der Verbindung zum MQTT-Broker und zum WIFI
    void setup();
    // Methode zum Überprüfen der Verbindung mit dem MQTT-Broker
    bool mqttConnectionStatus();
    // Herstellen der Verbindung mit dem MQTT-Broker anhand der Zugangsdaten
    void connectMqttBroker();
    // Update-Call für die Topics
    void loopMqtt();
    //  Herstellen der Verbindung mit dem WIFI anhand der Zugangsdaten
    void connectToWifi();
    //  Methode zum Überprüfen der Verbindung mit dem WIFI
    bool wifiConnectionStatus();
    //  Funktion für die Anzeige von Debuginformationen der WIFI-Verbindung über die serielle Schnittstelle
    void printDebugInformationWIFI();
    //  Funktion für die Anzeige von Debuginformationen der MQTT-Verbindung über die serielle Schnittstelle
    void printDebugInformationMQTT();
};

#endif