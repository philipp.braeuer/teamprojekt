#ifndef LIFT_H
#define LIFT_H
#include <Arduino.h>
#include <nRF24L01.h>
#include <RF24.h>

class lift
{
private:
    // Adresse des Lifts
    const uint8_t ADDRESS[5] = {210, 24, 21, 89, 35};

    // RF Channel 2.4 GHZ + CHANNEL * 10 MHz
    const uint8_t CHANNEL = 100;

    // Paketlänge eines Befehls
    const uint8_t PKT_SIZE = 6;

    // CE Pin des NRF24
    const uint8_t CE_PIN = 4;

    // CSN Pin des NRF24
    const uint8_t CSN_PIN = 5;

    // Anzahl an Wiederholungen für einen Befehl
    const uint8_t COMMAND_REPEATS = 1;

    RF24 radio{CE_PIN, CSN_PIN}; // CE, CSN

    // Sendet die Befehle an den Lift
    void sendCommand(uint8_t* buf);
public:
    lift();

    // Aufbau der Verbindung zum NR24
    void setupNRF24();

    // Methoden für das Senden der einzelnen Befehle entsprechend der Fernbedienung
    // Alle Methoden nutzen die sendCommand Methode
    void controlUp();
    void controlDown();
    void controlLeft();
    void controlRight();
    void controlSet();
    void controlMem1();
    void controlMem2();
    void controlButton1();
    void controlButton2();
    void controlButtonOk();
    
    // Funktion für die Anzeige von Debuginformationen über die serielle Schnittstelle
    void printDebugInformation();
};

#endif