#ifndef SERIAL_HANDLER_H
#define SERIAL_HANDLER_H
#include <Arduino.h>
#include <Preferences.h>
#include "mqttHandler.h"
#include "lift.h"

class serialHandler 
{
    private:
        // Baud-Rate für die Kommunikation über die serielle Schnittstelle
        const int BAUD_RATE = 115200;

        // Variable zum Zwischenspeichern der Eingabe über die serielle Schnittstelle
        String stringBuffer = ""; 

        // Lädt die gespeicherten Werte für die Zugangsdaten aus dem Flash
        void loadMemoryToValues();
        // Schreibt den aktuellen Inhalt von stringBuffer in den Flash des ESP32
        void writeStringBuffer(String address);
        // Methode zum Übergang in den Programmiermodus
        void enterProgrammingMode();
        // Methode zum Übergang in den Standard Betriebsmodus
        void enterStandardMode();
        // Hilfsfunktion für das Zwischenspeichern der bisher eingegebenen Zeichen des Benutzers
        // über die serielle Schnittstelle
        void enterValue(String address);
        
    public:
        // Referenzen auf Objekte der anderen Komponenten
        mqttHandler mqttBroker{};
        lift showcaseLift{};
        Preferences configuration{};

        serialHandler();

        // Ruft die Setup-Funktionen von mqttBroker und showcaseLift auf
        void setup();
};

#endif